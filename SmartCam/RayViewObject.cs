﻿using System;
using UnityEngine;

namespace SmartCam
{
    [System.Serializable]
    public class RayViewObject
    {
        private SmartThirdPersonCamera camera = null;
        private GameObject leftRay;
        private GameObject rightRay;
        private GameObject topRay;
        private GameObject bottomRay;



        public RayViewObject(SmartThirdPersonCamera camera)
        {
            this.camera = camera;
            InitRayViewObject();
        }
        

        
        void InitRayViewObject()
        {
            drawRay(leftRay,(camera.movingObjectParent.transform.position + camera.playerLeftMaxSide));
            drawRay(rightRay,(camera.movingObjectParent.transform.position + camera.playerRightMaxSide));
            drawRay(topRay,(camera.movingObjectParent.transform.position + camera.playerUpperMaxSide));
            drawRay(bottomRay,(camera.movingObjectParent.transform.position + camera.playerBottomMaxSide));
            
        }

        void drawRay(GameObject ray,Vector3 side)
        {
            GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
            ray = new GameObject();
            ray.AddComponent<MeshRenderer>();
            ray.AddComponent<BoxCollider>();
            ray.AddComponent<MeshFilter>();
            ray.GetComponent<MeshFilter>().mesh = temp.GetComponent<MeshFilter>().mesh;
            GameObject.Destroy(temp);
            
            ray.transform.localPosition = camera.transform.localPosition;
            ray.transform.parent = camera.transform;

            Vector3 direction = side - ray.transform.position;
            Vector3 newDirection = Vector3.RotateTowards(ray.transform.forward, direction, 1, 0.0f);

            ray.transform.rotation = Quaternion.LookRotation(newDirection);
            ray.transform.localScale = camera.movingObjectParent.transform.forward * direction.magnitude;

            if (ray.transform.localScale.z > 0)
            {
                ray.transform.localScale = new Vector3(0.005f, 0.005f, ray.transform.localScale.z);
            }

            ray.transform.localPosition = new Vector3(0, 0, 0) + (direction.normalized * direction.magnitude / 1.99f);
        }
    }
}