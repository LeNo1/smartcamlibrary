﻿using System;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

namespace SmartCam
{
    public class SmartThirdPersonCamera : MonoBehaviour
    {
        private static Stopwatch timer = new Stopwatch();
        
        public GameObject movingObjectParent = null;
        [SerializeField]
        private Vector3 cameraDirectionFromObject;
        [SerializeField] 
        private Vector3 cameraPositionRotationWithRadius;
        [SerializeField]
        private float distance = 3;
        [SerializeField]
        private Vector3 rotationAxisAngle;

        
        [SerializeField] 
        public Vector3 playerLeftMaxSide;
        [SerializeField] 
        public Vector3 playerRightMaxSide;
        [SerializeField] 
        public Vector3 playerUpperMaxSide;
        [SerializeField] 
        public Vector3 playerBottomMaxSide;

        private RayViewObject _rayViewObject;
        
        [SerializeField]
        private float moveSpeed=1;

        [SerializeField]
        private float rotationSpeed=1;


        private bool test = false;

        
        
        void Start()
        {
            this.movingObjectParent = this.transform.root.gameObject;
            this.InitCameraPosition();
        }

        void Update()
        {
            float rotationValue = Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed;
            
            Quaternion quaternion = Quaternion.Euler(0,rotationValue,0);
            
            movingObjectParent.transform.Rotate(new Vector3(0,rotationValue,0));
            
        }


        private void FixedUpdate()
        {
            if (Input.GetKey(KeyCode.W))
            {
                movingObjectParent.GetComponent<Rigidbody>().velocity = movingObjectParent.transform.forward * moveSpeed;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                movingObjectParent.GetComponent<Rigidbody>().velocity = movingObjectParent.transform.forward * (-1 * moveSpeed);

            }
            else
            {
                movingObjectParent.GetComponent<Rigidbody>().velocity = Vector3.zero;

            }
        }

        
        void InitCameraPosition()
        {
            Quaternion rotation = Quaternion.Euler(cameraPositionRotationWithRadius);
            Vector3 initPosition = cameraDirectionFromObject * distance;
            initPosition = rotation * initPosition;
            
            
            transform.forward = cameraDirectionFromObject * -1f;
            transform.localPosition = initPosition;
            transform.Rotate(rotationAxisAngle);
            
            _rayViewObject = new RayViewObject(this);
        }


        

        
        
        private void CheckIfObjectAreBetween()
        {
            if (transform.localPosition.magnitude >= distance)
            {
                RaycastHit[] hits = Physics.RaycastAll(transform.position ,transform.forward+transform.up*-1, distance/2);
            
                foreach (RaycastHit hit in hits)
                {
                    if (!hit.transform.gameObject.name.Equals(movingObjectParent.name))
                    {
                        Debug.Log($"Hitted {hit.transform.gameObject.name}");
                        //SetCameraPositionCloserToObject();
                        return;
                    }
                
                }
            }
            
        }

        private bool CheckIfObjectIsBigEnough(RaycastHit hitObject)
        {
            return false;
        }

        private void SetCameraPositionCloserToObject()
        {

            transform.localPosition = transform.localPosition.normalized + Vector3.forward*1/2;
        }
        
    }
}